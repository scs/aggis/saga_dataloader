# SaGA_dataloader



## Getting started

This dataloader enables to iterate frame by frame over the SaGA1 dataset and get all annotations, audio, video and gesture data from the dataset.

## Installation

Currently we don't have a prebuilt conda package for the dataloader, therefore you will have to import it into your project if needed.
The following dependencies have to be installed to use the dataloader
```
pip install librosa numba numpy opencv-python
```

## Data structure

The following data can be retrieved every frame (including the video frame data if enabled in the class constructor).
The keys of the retrieved dictionary are the same as in this explanation

Video data for each frame
```
Video
```

Audio data for each frame
```
Audio 
```

The text for the spoken word during the current frame. Both from the speaker as well as the listener.
```
R_S_Text
F_S_Text
```

The semantic information of the speech (S) and the gestures (G)
```
R_S_Semantic
R_G_Left_Semantic
R_G_Right_Semantic
```

Entity from the semantic features clustered by discussed topic in the text
This is a manual annotation by me and can help to understand the "entity" tag in the semantic information
```
R_S_Semantic_Entity
```

Current interaction phase (prep,stroke,retr,post.hold)
```
R_S_Left_Phase
R_S_Right_Phase
```

Current gesture phrase (iconic, deictic, etc.) for both speaker and listener
```
R_G_Left_Phrase
R_G_Right_Phrase
F_G_Left_Phrase
F_G_Right_Phrase
```

All of the Hand Annotations
```
LeftHandShape
LeftPalmDirection
LeftBackOfHandDirection
LeftBackOfHandDirectionMovement
LeftWristPosition
LeftWristDistance
LeftPathOfWristLocation
LeftWristLocationMovementDirection
LeftExtent
LeftPractice

RightHandShape
RightPalmDirection
RightBackOfHandDirection
RightBackOfHandDirectionMovement
RightWristPosition
RightWristDistance
RightPathOfWristLocation
RightWristLocationMovementDirection
RightExtent
RightPractice
```
The gesture data for each frame.
This is currently a dictionary with an increasing id (starting with 0) for each person in the frame.
Altough the algorithm tries to use the same id for the same person in subsequent frames, sometimes the tracking breaks and a new person id is created.
The structure is as follows:
```
dict [id as key] {
    dict {
        id
        image_dimensions
        current_2d_pose = 136x2  - keypoints tracked on the 2d image
        current_2d_score = 136x1 - How certain the algorithm is about the tracking
        current_3d_pose = 59x3   - 3d keypoints tracked in image space
        current_3d_traj = 3      - 3d trajectory of the person. This can be a bit unreliable and drift over time  
    }
}
```

For the 59x3 poses the data is combined from the 17 points of the body skeleton and 2*21 points for the hands.

![](points 17.png)
![](hand.jpg)


## Project status
Currently mantained by Hendric Voß <hvoss@techfak.uni-bielefeld.de>