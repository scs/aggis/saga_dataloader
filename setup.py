# -*- coding: utf-8 -*-
from setuptools import setup, find_packages


NAME = "saga_dataloader"
AUTHOR = 'Hendric Voß'
DESCRIPTION = 'A handy iterator to receive frame by frame data from the SaGA elan files hosted on moleman.'
URL = 'https://gitlab.ub.uni-bielefeld.de/scs/aggis/saga_dataloader'
EMAIL = "hvoss@techfak.uni-bielefeld.de"
VERSION = 0.1

REQUIRED = ["pympi-ling","opencv","librosa","numpy"]


setup(
    name=NAME,
    version=VERSION,
    description=DESCRIPTION,
    author=AUTHOR,
    author_email=EMAIL,
    url=URL,
    # packages=find_packages(),
    py_modules=["saga_dataloader"],
    zip_safe=False,
    install_requires=REQUIRED,
    license='MIT'
)
